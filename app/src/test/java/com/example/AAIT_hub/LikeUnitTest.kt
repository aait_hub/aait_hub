package com.example.aait_hub

import com.example.aait_hub.model.Like
import com.example.aait_hub.model.Post
import com.example.aait_hub.model.Student
import com.example.aait_hub.utils.Utilities
import org.junit.Assert
import org.junit.Test
import java.util.*

class LikeUnitTest {
    @Test
    fun testVote(){
        val post = Post(1, 1, 1, "", "", "", Date(), 1, false)
        val post1 = Post(0, 1, 1, "", "", "", Date(), 1, false)

        val stud = Student(1, "firstName", "lastName", "username", "password", "bio", "software")
        val stud1 = Student(0, "firstName", "lastName", "username", "password", "bio", "software")

        val like = Like(0, stud, post, Date())
        val like1 = Like(1, stud1, post, Date())
        val like2 = Like(1, stud, post1, Date())
        val like3 = Like(1, stud, post, Date())

        Assert.assertFalse(Utilities.validateLike(like))
        Assert.assertFalse(Utilities.validateLike(like1))
        Assert.assertFalse(Utilities.validateLike(like2))
        Assert.assertTrue(Utilities.validateLike(like3))
    }
}