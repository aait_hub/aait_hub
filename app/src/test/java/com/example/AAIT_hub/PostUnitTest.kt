package com.example.aait_hub

import com.example.aait_hub.model.Post
import com.example.aait_hub.utils.Utilities
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class PostUnitTest {

    @Test
    fun testPostContent() {
        val testCase1 = ""
        val testCase2 = "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
        val testCase4 = "post content"

        assertFalse(Utilities.validatePostContent(testCase1))
        assertFalse(Utilities.validatePostContent(testCase2))
        assertTrue(Utilities.validatePostContent(testCase4))
    }

    @Test
    fun testPostTitle() {
        val testCase1 = ""
        val testCase2 = "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
        val testCase4 = "post content"

        assertFalse(Utilities.validatePostTitle(testCase1))
        assertFalse(Utilities.validatePostTitle(testCase2))
        assertTrue(Utilities.validatePostTitle(testCase4))
    }

    @Test
    fun testPost(){
        val post = Post(1, 1, 1, "title", "test", "test.png", Date(), 1, false )
        val post1 = Post(0, 1, 1, "title", "test", "test.png", Date(), 1, false )
        val post2 = Post(1, 0, 1, "title", "test", "test.png", Date(), 1, false )
        val post3 = Post(1, 1, -1, "title", "test", "test.png", Date(), 1, false )
        val post4 = Post(1, 1, 1, "title", "test", "test.png", Date(), -1, false )

        assertFalse(Utilities.validatePost(post1))
        assertFalse(Utilities.validatePost(post2))
        assertFalse(Utilities.validatePost(post3))
        assertFalse(Utilities.validatePost(post4))
        assertTrue(Utilities.validatePost(post))
    }
}
