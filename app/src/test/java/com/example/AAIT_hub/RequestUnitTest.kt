package com.example.aait_hub

import com.example.aait_hub.model.Request
import com.example.aait_hub.utils.Utilities
import org.junit.Assert
import org.junit.Test
import java.util.*

class RequestUnitTest {
    @Test
    fun validateRequest(){
        val request = Request(1, 1, 1, Date())
        val request1 = Request(0, 1, 1, Date())
        val request2 = Request(1, 0, 1, Date())
        val request3 = Request(1, 1, 0, Date())

        Assert.assertFalse(Utilities.validateRequest(request1))
        Assert.assertFalse(Utilities.validateRequest(request2))
        Assert.assertFalse(Utilities.validateRequest(request3))
        Assert.assertTrue(Utilities.validateRequest(request))
    }
}