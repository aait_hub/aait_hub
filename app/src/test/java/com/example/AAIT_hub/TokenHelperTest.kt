package com.example.aait_hub

import org.junit.Assert
import org.junit.Test

class TokenHelperTest {
    @Test
    fun logInParametersTest(){
        val SPK = "shared_preference_key"
        val UIK = "user_id_key"
        val UTK = "user_type_key"
        val TSK = "token_string_key"
        val TI = "token_id_key"
        val ED = "expire_date_key"

        Assert.assertEquals(SPK, SHAREDPREFERENCEKEY)
        Assert.assertEquals(UIK, USERIDKEY)
        Assert.assertEquals(UTK, USERTYPEKEY)
        Assert.assertEquals(TSK, TOKENSTRINGKEY)
        Assert.assertEquals(TI, TOKENID)
        Assert.assertEquals(ED, EXPIREDATE)
    }
}