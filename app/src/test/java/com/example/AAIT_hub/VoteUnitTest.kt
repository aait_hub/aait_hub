package com.example.aait_hub

import com.example.aait_hub.model.Post
import com.example.aait_hub.model.Student
import com.example.aait_hub.model.Vote
import com.example.aait_hub.utils.Utilities
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class VoteUnitTest {

    @Test
    fun testVote(){
        val post = Post(1, 1, 1, "", "", "", Date(), 1, false)
        val post1 = Post(0, 1, 1, "", "", "", Date(), 1, false)

        val stud = Student(1, "firstName", "lastName", "username", "password", "bio", "software")
        val stud1 = Student(0, "firstName", "lastName", "username", "password", "bio", "software")

        val vote = Vote(0, stud, post, Date())
        val vote1 = Vote(1, stud1, post,Date())
        val vote2 = Vote(1, stud, post1,Date())
        val vote3 = Vote(1, stud, post,Date())

        assertFalse(Utilities.validateVote(vote))
        assertFalse(Utilities.validateVote(vote1))
        assertFalse(Utilities.validateVote(vote2))
        assertTrue(Utilities.validateVote(vote3))
    }
}
