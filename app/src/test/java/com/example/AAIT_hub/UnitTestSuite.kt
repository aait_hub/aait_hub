package com.example.aait_hub

import com.example.aait_hub.utils.Utilities
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@RunWith(Suite::class)
@Suite.SuiteClasses(PostUnitTest::class, VoteUnitTest::class, LikeUnitTest::class, ProjectUnitTest::class, TokenHelperTest::class, RequestUnitTest::class)
class UnitTestSuite {

}
