package com.example.aait_hub

import com.example.aait_hub.model.Project
import com.example.aait_hub.utils.Utilities
import org.junit.Assert
import org.junit.Test
import java.util.*

class ProjectUnitTest {

    @Test
    fun testProject(){
        val project = Project(1, 1, "name", "description", Date())
        val project1 = Project(0, 1,"name", "description", Date())
        val project2 = Project(1, 0,"name", "description", Date())

        Assert.assertFalse(Utilities.validateProject(project1))
        Assert.assertFalse(Utilities.validateProject(project2))
        Assert.assertTrue(Utilities.validateProject(project))
    }

    @Test
    fun testProjectName() {
        val project = Project(1, 1, "name", "description", Date())
        val project1 = Project(1, 1,"", "description", Date())
        val project2 = Project(1, 1,"name name name name name name name name name name name name name name name", "description", Date())

        Assert.assertFalse(Utilities.validateProjectName(project1))
        Assert.assertFalse(Utilities.validateProjectName(project2))
        Assert.assertTrue(Utilities.validateProjectName(project))
    }

    @Test
    fun testProjectDescription() {
        val project = Project(1, 1, "name", "description", Date())
        val project1 = Project(0, 1,"name", "", Date())
        val project2 = Project(1, 0,"name", "description description description description description description description description description", Date())

        Assert.assertFalse(Utilities.validateProjectDescription(project1))
        Assert.assertFalse(Utilities.validateProjectDescription(project2))
        Assert.assertTrue(Utilities.validateProjectDescription(project))
    }
}