package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.viewModel.ProjectViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_my_projects.view.*

class MyProjectsFragment : Fragment() {
    private lateinit var projectRecyclerView: RecyclerView
    private lateinit var context1: Context
    private lateinit var projectViewModel: ProjectViewModel
    private lateinit var myProjectRecyclerViewAdapter: MyProjectsRecyclerViewAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context1 = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        projectViewModel = activity?.run {
            ViewModelProviders.of(this).get(ProjectViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        myProjectRecyclerViewAdapter = MyProjectsRecyclerViewAdapter(context1)

        projectViewModel.myProject.observe(this, Observer {
                projects -> projects?.let {myProjectRecyclerViewAdapter.setMyProjects(projects.reversed())}
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).show()

        val view = inflater.inflate(R.layout.fragment_my_projects, container, false)
        projectRecyclerView = view.my_project_recycler_view
        projectRecyclerView.layoutManager = LinearLayoutManager(activity)
        projectRecyclerView.adapter = myProjectRecyclerViewAdapter
        projectRecyclerView.setHasFixedSize(true)

        projectViewModel.setMyProjects(projectViewModel.getAllProjects())

        return view
    }
}
