package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface StudentDao {
    @Query("SELECT * FROM USER")
    fun getAllStudents():LiveData<List<Student>>

    @Query("SELECT * FROM USER WHERE userName=:uName")
    fun getStudentByUserName(uName:String):LiveData<Student>

    @Query("SELECT * FROM USER WHERE studentId=:id")
    fun getStudentById(id:Int):LiveData<Student>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addStudent(student: Student):Long

    @Update
    fun updateStudent(student: Student):Int

    @Delete
    fun removeStudent(student: Student):Int
}