package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PostReturnDao {
    @Query("SELECT * FROM PostReturn")
    fun getAllPostReturns(): LiveData<List<PostReturn>>

    @Query("SELECT * FROM PostReturn WHERE p_postOwnerId=:ownerId")
    fun getAllMyPostReturns(ownerId:Int): LiveData<List<PostReturn>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPostReturns(postReturns:List<PostReturn>)

    @Query("DELETE FROM PostReturn")
    fun removeAllPostReturns():Int
}