package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "PostReturn")
data class PostReturn (
    @PrimaryKey var id:Int,
    @Embedded(prefix = "p_") val post:Post,
    val vote:Int,
    val like:Int,
    val iLiked:Boolean,
    val iVoted:Boolean,
    val iJoined:Boolean
):Serializable