package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.viewModel.PostViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_show_my_post.view.*

class showMyPostFragment : Fragment() {
    private lateinit var showPostRecyclerView: RecyclerView
    private lateinit var context1: Context
    private lateinit var postViewModel: PostViewModel
    private lateinit var myPostRecyclerViewAdapter: MyPostRecyclerViewAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context1 = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postViewModel = activity?.run {
            ViewModelProviders.of(this).get(PostViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        myPostRecyclerViewAdapter = MyPostRecyclerViewAdapter(context1)

        postViewModel.myPosts.observe(this, Observer { posts ->
            posts?.let { myPostRecyclerViewAdapter.setMyPosts(posts.reversed()) }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_show_my_post, container, false)

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).hide()

        showPostRecyclerView = view.show_my_post_recycler_view
        showPostRecyclerView.layoutManager = LinearLayoutManager(activity)
        showPostRecyclerView.adapter = myPostRecyclerViewAdapter
        showPostRecyclerView.setHasFixedSize(true)

        postViewModel.setMyPostReturn(postViewModel.getMyPosts(context1))

        return view
    }


}
