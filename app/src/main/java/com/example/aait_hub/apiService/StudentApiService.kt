package com.example.aait_hub.apiService

import com.example.aait_hub.model.Student
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface StudentApiService {
    @GET("students")
    fun getAllStudentsAsync(): Deferred<Response<List<Student>>>

    @GET("students/{userName}")
    fun getStudentByUserNameAsync(@Path("userName") userName:String): Deferred<Response<Student>>

    @GET("students/{id}")
    fun getStudentByIdAsync(@Path("id") id:Int): Deferred<Response<Student>>

    @POST("students")
    fun addStudentAsync(@Body student: Student): Deferred<Response<Void>>

    @PUT("students/{id}")
    fun updateStudentAsync(@Path("id") id:Int, @Body student: Student): Deferred<Response<Void>>

    @DELETE("students/{id}")
    fun removeStudentAsync(@Path("id") id:Int): Deferred<Response<Void>>
}