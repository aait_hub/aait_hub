package com.example.aait_hub.apiService

import com.example.aait_hub.model.token

data class RequestT(
    val token: token,
    val postId:Int
)