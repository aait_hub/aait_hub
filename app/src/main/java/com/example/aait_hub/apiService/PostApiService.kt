package com.example.aait_hub.apiService

import com.example.aait_hub.model.Post
import com.example.aait_hub.model.PostReturn
import com.example.aait_hub.model.token
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface PostApiService {

    @GET("Post/")
    fun getAllPostsAsync(@Query("tokenS") tokenString:String):Deferred<Response<List<PostReturn>>>

    @GET("Post/{id}/")
    fun getPostByPostIdAsync(@Path("id") id:Int, @Query("tokenS") tokenString:String):Deferred<Response<PostReturn>>

    @GET("Post/{ownerId}")
    fun getPostByOwnerId(@Path("ownerId") ownerId:Int):Deferred<Response<List<PostReturn>>>

    @GET("Post/{postType}")
    fun getPostByPostType(@Path("postType") postType: Int):Deferred<Response<List<Post>>>

    @POST("Post")
    fun addPostAsync(@Body postT: PostT):Deferred<Response<Void>>

    @PUT("Post/{id}")
    fun updatePostAsync(@Path("id") id:Int, @Body postT: PostT):Deferred<Response<Void>>

    @DELETE("Post/{id}")
    fun removePostAsync(@Path("id") id:Int, @Query("tokenS") tokenS: String):Deferred<Response<Void>>
}