package com.example.aait_hub.apiService

import com.example.aait_hub.model.Request
import com.example.aait_hub.model.token
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface RequestApiService {

    @GET("Join")
    fun getAllRequestAsync(@Query("tokenS") tokenS: String): Deferred<Response<List<Request>>>

    @POST("Join/{id}")
    fun addRequestAsync(@Path("id")id: Int , @Body token: token): Deferred<Response<Void>>

    @DELETE("Join/{id}")
    fun removeRequestAsync(@Path("id")id: Int , @Query("tokenS")tokenS: String): Deferred<Response<Void>>
}