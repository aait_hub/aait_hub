package com.example.aait_hub.apiService

import com.example.aait_hub.model.Post
import com.example.aait_hub.model.token

data class PostT(
    val token:token,
    val post: Post
)