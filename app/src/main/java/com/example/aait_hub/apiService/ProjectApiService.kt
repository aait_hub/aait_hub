package com.example.aait_hub.apiService

import com.example.aait_hub.model.Project
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ProjectApiService {
    @GET("Project")
    fun getProjectsByStudentIdAsync(@Query("tokenS") tokenS: String): Deferred<Response<List<Project>>>

    @GET("Project/{projectId}")
    fun getProjectsByStudentIdAndProjectIdAsync(@Path("projectId") projectId:Int, @Query("tokenS") tokenS: String): Deferred<Response<Project>>

    @POST("Project")
    fun addProjectAsync(@Body postT: RequestT): Deferred<Response<Void>>

    @DELETE("Project")
    fun removeProjectAsync(@Query("tokenS") tokenS:String, @Query("postId") postId: Int): Deferred<Response<Void>>
}