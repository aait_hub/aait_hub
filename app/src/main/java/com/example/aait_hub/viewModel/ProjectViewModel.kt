package com.example.aait_hub.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.aait_hub.model.AAIThubDatabase
import com.example.aait_hub.model.Project
import com.example.aait_hub.repository.ProjectRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProjectViewModel(application: Application):AndroidViewModel(application) {
    private var projectRepository: ProjectRepository

    var myProject: LiveData<List<Project>>

    init {
        Log.d("project view model", "Created")
        val database = AAIThubDatabase.getDatabase(application)
        projectRepository = ProjectRepository(application, database.projectDao())

        myProject = projectRepository.getProjectsAsync()
    }

    fun setMyProjects(projects:LiveData<List<Project>>){
        myProject = projects
    }

    fun getAllProjects():LiveData<List<Project>>{
        return projectRepository.getProjectsAsync()
    }

    fun getProjectByStudentId(): LiveData<List<Project>>{
        return projectRepository.getAllProjects()
    }

    fun addProject(postId: Int)= viewModelScope.launch(Dispatchers.IO){
        projectRepository.addProject(postId)
    }

    fun updateProject(project: Project)= viewModelScope.launch(Dispatchers.IO){
        projectRepository.updateProject(project)
    }

    fun removeAllProjectFromCache(){
        projectRepository.removeAllProjects()
    }
}