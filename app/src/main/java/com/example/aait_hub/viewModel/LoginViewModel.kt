package com.example.aait_hub.viewModel

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.aait_hub.apiService.StudentApiService
import com.example.aait_hub.model.AAIThubDatabase
import com.example.aait_hub.model.Student
import com.example.aait_hub.repository.StudentRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import androidx.lifecycle.MutableLiveData
import com.example.aait_hub.apiService.LoginApiService
import com.example.aait_hub.apiService.LoginData
import com.example.aait_hub.model.token
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import kotlinx.coroutines.*


class LoginViewModel(application: Application):AndroidViewModel(application) {
    private var studentRepository: StudentRepository
    private var loginApiService:LoginApiService = RetrofitProvider.getRetrofit().create(LoginApiService::class.java)
    private var connectivityChecker: ConnectivityChecker = ConnectivityChecker(application)

    init {
        val database = AAIThubDatabase.getDatabase(application)
        studentRepository = StudentRepository(application, database.studentDao())
    }

    fun loginAsync(loginData: LoginData): token? {
        var token: token? = null
        if(connectivityChecker.isConnectedToNetwork()){
            runBlocking(Dispatchers.IO) {
                Log.d("response successful", "in login data")
                val response:Response<token> = loginApiService.loginAsync(loginData).await()

                if(response.isSuccessful){
                    token = response.body()
                    Log.d("response successful", "${token?.tokenString}")
                }
                else Log.d("response not successful", "${response.code()}")
            }
        }
        else Log.d("loginAsync in viewModel", "not connected")
        return token
    }

    fun logOutAsync(tokenS:String):Boolean{
        var isSuccessful = false
        if(connectivityChecker.isConnectedToNetwork()){
            isSuccessful = true
            GlobalScope.async(Dispatchers.IO) {
                val response = loginApiService.logoutAsync(tokenS).await()
            }
        }
        return isSuccessful
    }

}