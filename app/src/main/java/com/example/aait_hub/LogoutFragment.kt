package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.aait_hub.utils.TokenHelper


class LogoutFragment : Fragment() {
    private lateinit var _context:Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        _context = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TokenHelper.removeTokenFromSharedPreference(_context)
        findNavController().navigate(R.id.action_logout_to_loginFragment)
    }
}
