package com.example.aait_hub.repository

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.aait_hub.apiService.StudentApiService
import com.example.aait_hub.model.Student
import com.example.aait_hub.model.StudentDao
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Response

class StudentRepository(private val context: Context, private val studentDao: StudentDao) {
    private var studentApiService: StudentApiService = RetrofitProvider.getRetrofit().create(StudentApiService::class.java)
    private var connectivityChecker: ConnectivityChecker = ConnectivityChecker(context)

    fun getAllStudents(): LiveData<List<Student>>{
        if(connectivityChecker.isConnectedToNetwork()) return getAllStudentsAsync()
        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
        return studentDao.getAllStudents()
    }

    fun getStudentByUserName(uName:String):LiveData<Student>{
        if(connectivityChecker.isConnectedToNetwork()) return getStudentByUserNameAsync(uName)
        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
        return studentDao.getStudentByUserName(uName)
    }

    fun getStudentById(id:Int):LiveData<Student>{
        if(connectivityChecker.isConnectedToNetwork()) return getStudentByIdAsync(id)
        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
        return studentDao.getStudentById(id)
    }

    fun addStudent(student: Student):Boolean{
        return addStudentAsync(student)
    }

    fun updateStudent(student: Student):Int{
        return studentDao.updateStudent(student)
    }

    fun removeStudent(student: Student):Boolean{
        return removeStudentAsync(student.studentId)
    }

    //API SERVICES
    private fun getAllStudentsAsync():LiveData<List<Student>>{
        var students:List<Student>? = null

        runBlocking(Dispatchers.IO) {
            val response: Response<List<Student>> = studentApiService.getAllStudentsAsync().await()
            if(response.isSuccessful){
                students = response.body()
            }
        }
        return MutableLiveData<List<Student>>(students)
    }

    private fun getStudentByUserNameAsync(uName: String): LiveData<Student>{
        var student: Student? = null
        runBlocking(Dispatchers.IO) {
            val response: Response<Student> = studentApiService.getStudentByUserNameAsync(uName).await()

            if(response.isSuccessful)
                student = response.body()
        }
        return MutableLiveData<Student>(student)
    }

    private fun getStudentByIdAsync(id: Int): LiveData<Student>{
        var student: Student? = null
        runBlocking(Dispatchers.IO) {
            val response: Response<Student> = studentApiService.getStudentByIdAsync(id).await()

            if(response.isSuccessful)
                student = response.body()
        }
        return MutableLiveData<Student>(student)
    }

    private fun addStudentAsync(student: Student):Boolean{
        var isSuccessful = false

        runBlocking(Dispatchers.IO) {
            val response: Response<Void> = studentApiService.addStudentAsync(student).await()
            if(response.isSuccessful)
                isSuccessful = true
        }
        return isSuccessful
    }

    private fun removeStudentAsync(studentId: Int):Boolean{
        var isSuccessful = false

        GlobalScope.launch((Dispatchers.IO)) {
            val response: Response<Void> = studentApiService.removeStudentAsync(studentId).await()
            if(response.isSuccessful)
                isSuccessful = true
        }
        return isSuccessful
    }
}