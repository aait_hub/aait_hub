package com.example.aait_hub

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.databinding.MyProjectsRecyclerViewItemBinding
import com.example.aait_hub.model.*
import kotlinx.android.synthetic.main.fragment_my_projects.view.*

class MyProjectsRecyclerViewAdapter(private val context: Context): RecyclerView.Adapter<MyProjectsRecyclerViewAdapter.MyProjectsViewHolder>() {

    private var projects: List<Project> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyProjectsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = MyProjectsRecyclerViewItemBinding.inflate(inflater)

        recyclerViewItem.root.setOnClickListener {
            val recyclerView:RecyclerView = parent.my_project_recycler_view
            val itemPosition = recyclerView.getChildLayoutPosition(recyclerViewItem.root)
            val project = projects[itemPosition]
            val projectBundle = Bundle()
            projectBundle.putSerializable("project", project)
            it.findNavController().navigate(R.id.detailMessageFragment, projectBundle)
        }

        return MyProjectsViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return projects.count()
    }

    override fun onBindViewHolder(holder: MyProjectsViewHolder, position: Int) {
        val project = projects[position]
        holder.bind(project)
    }

    internal fun setMyProjects(projects:List<Project>){
        this.projects = projects
        notifyDataSetChanged()
    }

    class MyProjectsViewHolder(private val itemBinding: MyProjectsRecyclerViewItemBinding):RecyclerView.ViewHolder(itemBinding.root){
        fun bind(project: Project){
            itemBinding.itemBindingProject = project
        }
    }
}