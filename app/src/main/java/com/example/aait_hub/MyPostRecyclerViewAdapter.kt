package com.example.aait_hub

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.model.PostReturn
import com.example.aait_hub.databinding.MyPostRecyclerViewItemBinding

class MyPostRecyclerViewAdapter(private val context: Context):RecyclerView.Adapter<MyPostRecyclerViewAdapter.MyPostViewHolder>() {

    private var postReturns: List<PostReturn> = emptyList()
    private lateinit var listener: PostRecyclerViewAdapter.OnAdapterButtonsClickManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = MyPostRecyclerViewItemBinding.inflate(inflater)

        if (context is PostRecyclerViewAdapter.OnAdapterButtonsClickManager){
            listener = context
        }

        return MyPostViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return postReturns.count()
    }

    override fun onBindViewHolder(holder: MyPostViewHolder, position: Int) {
        val postReturn = postReturns[position]
        holder.bind(postReturn, listener)
    }

    internal fun setMyPosts(postReturns:List<PostReturn>){
        this.postReturns = postReturns
        notifyDataSetChanged()
    }

    class MyPostViewHolder(private val itemBinding: MyPostRecyclerViewItemBinding): RecyclerView.ViewHolder(itemBinding.root){
        fun bind(postReturn: PostReturn, listener:PostRecyclerViewAdapter.OnAdapterButtonsClickManager){
            itemBinding.postReturnBindingItem = postReturn
            itemBinding.postClickHandler = PostRecyclerViewAdapter.PostClickHandler(postReturn.post, listener)
        }
    }
}