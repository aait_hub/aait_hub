package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.aait_hub.databinding.FragmentDetailPostBinding
import com.example.aait_hub.model.Post
import com.example.aait_hub.model.PostReturn
import com.google.android.material.floatingactionbutton.FloatingActionButton

class DetailPostFragment : Fragment() {

    private lateinit var listener: PostRecyclerViewAdapter.OnAdapterButtonsClickManager

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PostRecyclerViewAdapter.OnAdapterButtonsClickManager){
            listener = context
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).show()

        var binding:FragmentDetailPostBinding  = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_post, container, false)
        var view = binding.root

        //binding.loginViewModel = OnLoginClickHandler("", "")
        val postReturn = arguments?.getSerializable("postReturn") as PostReturn
        binding.detailPostReturnBindingItem = postReturn
        binding.isProjectRequestPost = postReturn.post.postType == 2
        binding.detailPostReturnClickHandler = PostRecyclerViewAdapter.PostClickHandler(postReturn.post, listener)

        return view
    }
}
