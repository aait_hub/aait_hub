package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.aait_hub.databinding.FragmentEditPostBinding
import com.example.aait_hub.model.Post
import com.google.android.material.floatingactionbutton.FloatingActionButton

class EditPostFragment : Fragment() {

    private lateinit var listener: PostRecyclerViewAdapter.OnAdapterButtonsClickManager
    private lateinit var _context:Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is PostRecyclerViewAdapter.OnAdapterButtonsClickManager){
            listener = context
            _context = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).hide()

        var post = arguments?.getSerializable("post") as Post
        var binding: FragmentEditPostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_post, container, false)
        var view = binding.root
        binding.post = post
        binding.editPostClickHandler = EditPostClickHandler(listener)

        return view
    }

    class EditPostClickHandler(private val listener:PostRecyclerViewAdapter.OnAdapterButtonsClickManager){
        fun onSaveClick(post: Post){
            listener.onSaveButtonClicked(post)
        }
    }
}
