package com.example.aait_hub

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.model.Message
import kotlinx.android.synthetic.main.detail_message_recycler_view_item.view.*
import com.example.aait_hub.databinding.DetailMessageRecyclerViewItemBinding

class DetailMessageRecyclerViewAdapter(private val context: Context):RecyclerView.Adapter<DetailMessageRecyclerViewAdapter.DetailMessageViewHolder>() {
    private var detailMessages: List<Message> = emptyList()
    //private lateinit var listener:OnMessageItemClickManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailMessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = DetailMessageRecyclerViewItemBinding.inflate(inflater)

        return DetailMessageViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return detailMessages.count()
    }

    override fun onBindViewHolder(holder: DetailMessageViewHolder, position: Int) {
        val message = detailMessages[position]
        holder.bind(message)

    }

    internal fun setMessages(detailMessages:List<Message>?){
        if(detailMessages.isNullOrEmpty())
            this.detailMessages = emptyList()
        else
            this.detailMessages = detailMessages
        notifyDataSetChanged()
    }

    class DetailMessageViewHolder(private val itemBinding: DetailMessageRecyclerViewItemBinding):RecyclerView.ViewHolder(itemBinding.root){
        fun bind(message: Message){
            itemBinding.itemBindingDetailMessage = message.messageContent
        }
    }
}