package com.example.aait_hub.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.aait_hub.model.AAIThubDatabase
import com.example.aait_hub.model.Message
import com.example.aait_hub.model.Project
import com.example.aait_hub.model.Student
import com.example.aait_hub.repository.MessageRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class MessageViewModel(application: Application):AndroidViewModel(application) {
    private var messageRepository:MessageRepository

    val detailMessages: LiveData<List<Message>>

    init {
        Log.d("Message view model", "Created")
        val database = AAIThubDatabase.getDatabase(application)
        messageRepository = MessageRepository(database.messageDao())

        detailMessages = messageRepository.getMessagesByProjectId(0)
    }

    fun onSendMessageButtonClick(messageContent:String, project:Project){
        addMessage(getMessage(messageContent, project))
        //notifyDataSetChanged()
    }
    private fun getMessage(content:String, project:Project):Message{
        val student = Student(1, "fName","lName","uName", "pass", "bio", "software")
        return Message(0, project, student, content, Date())
    }

    fun getAllMessages():LiveData<List<Message>> {
        return messageRepository.getAllMessages()
    }

    fun getMessagesByStudentId(studentId:Int): LiveData<List<Message>>{
        return messageRepository.getMessagesByStudentId(studentId)
    }

    fun getMessagesByProjectId(projectId:Int): LiveData<List<Message>>{
        return messageRepository.getMessagesByProjectId(projectId)
    }

    fun getMessageByEachProject():LiveData<List<Message>>{
        var messages = getAllMessages().value?.reversed()
        var finalMessages:MutableList<Message> = emptyList<Message>().toMutableList()
        var foundProjectIds:MutableList<Int> = emptyList<Int>().toMutableList()
        if(messages != null){
            for(message:Message in messages){
                if(!foundProjectIds.contains(message.project.projectId)){
                    //find project members by project id and check if current user is in one of them
                    finalMessages.add(message)
                    foundProjectIds.add(message.messageId)
                }
            }
        }
        return MutableLiveData(finalMessages)
    }

    fun addMessage(message: Message)= viewModelScope.launch(Dispatchers.IO){
        messageRepository.addMessage(message)
    }

    fun updateMessage(message: Message)= viewModelScope.launch(Dispatchers.IO){
        messageRepository.updateMessage(message)
    }

    fun removeMessgae(message: Message)= viewModelScope.launch(Dispatchers.IO){
        messageRepository.removeMessgae(message)
    }

    fun removeAllMessageFromCache(){
        messageRepository.removeAllMessages()
    }
}