package com.example.aait_hub.viewModel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.aait_hub.apiService.PostT
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.model.*
import com.example.aait_hub.repository.*
import com.example.aait_hub.utils.TokenHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.*

class PostViewModel(application: Application):AndroidViewModel(application) {
    private var postRepository: PostRepository
    private var likeRepository: LikeRepository
    private var voteRepository: VoteRepository
    private var projectRepository: ProjectRepository

    var postReturns:LiveData<List<PostReturn>>
    var myPosts:LiveData<List<PostReturn>>

    init {
        val database = AAIThubDatabase.getDatabase(application)
        postRepository = PostRepository(application,database.postDao(), database.postReturnDao())
        likeRepository = LikeRepository(application, database.likeDao())
        voteRepository = VoteRepository(application, database.voteDao())
        projectRepository = ProjectRepository(application, database.projectDao())

        postReturns = postRepository.getAllPostsAsync()
        myPosts = postRepository.getPostByOwnerId(TokenHelper.getTokenFromSharedPreference(application).userId)
    }

    //POST
    fun setPostReturn(posts:LiveData<List<PostReturn>>){
        postReturns = posts
    }

    fun setMyPostReturn(posts:LiveData<List<PostReturn>>){
        myPosts = posts
    }

    fun getAllPosts():LiveData<List<PostReturn>>{
        return postRepository.getAllPostsAsync()
    }

    fun getMyPosts(context:Context):LiveData<List<PostReturn>>{
        return postRepository.getPostByOwnerId(TokenHelper.getTokenFromSharedPreference(context).userId)
    }

    fun addPost(postT: PostT) = viewModelScope.launch(Dispatchers.IO){
        postRepository.addPostAsync(postT)
        postRepository.refreshPost()
    }

    fun updatePost(post: Post) = viewModelScope.launch(Dispatchers.IO){
        postRepository.updatePostAsync(PostT(TokenHelper.getTokenFromSharedPreference(getApplication()),post))
    }

    fun removePost(postId: Int) = viewModelScope.launch(Dispatchers.IO){
        postRepository.removePostAsync(postId, TokenHelper.getTokenFromSharedPreference(getApplication()))
    }

    //Vote
    fun addVote(voteT: RequestT) = viewModelScope.launch(Dispatchers.IO){
        if(voteRepository.addVote(voteT)) postRepository.refreshPost()
    }

    fun removeVote(voteT: RequestT) = viewModelScope.launch(Dispatchers.IO){
        if(voteRepository.removeVote(voteT)) postRepository.refreshPost()
    }

    //LIKE
    fun addLike(likeT: RequestT) = viewModelScope.launch(Dispatchers.IO) {
        if(likeRepository.addLike(likeT)) postRepository.refreshPost()
    }

    fun removeLike(likeT: RequestT) = viewModelScope.launch(Dispatchers.IO){
        if(likeRepository.removeLike(likeT)) postRepository.refreshPost()
    }

    //PROJECT
//    fun addProject(project: Project) = viewModelScope.launch (Dispatchers.IO){
//        projectRepository.addProject(project)
//    }

//    fun getProjectByPostId(postId:Int):LiveData<Project>{
//        return projectRepository.getProjectByPostId(postId)
//    }
}