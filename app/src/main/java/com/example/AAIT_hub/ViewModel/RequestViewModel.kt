package com.example.aait_hub.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.model.AAIThubDatabase
import com.example.aait_hub.model.Request
import com.example.aait_hub.repository.RequestRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RequestViewModel(application: Application): AndroidViewModel(application) {
    private var requestRepository: RequestRepository

    var requests:LiveData<List<Request>>

    init {
        Log.d("request view model", "Created")
        val database = AAIThubDatabase.getDatabase(application)
        requestRepository = RequestRepository(database.requestDao(), application)
        requests = MutableLiveData<List<Request>>()//requestRepository.getAllRequests()//current logged in user
    }

    //REQUEST
    fun addRequest(request: RequestT) = viewModelScope.launch ( Dispatchers.IO ){
        requestRepository.addRequest(request)
    }

    fun removeRequest(request: RequestT) = viewModelScope.launch ( Dispatchers.IO ){
        requestRepository.removeRequest(request)
    }

    fun getAllRequests():LiveData<List<Request>>{
        return requestRepository.getAllRequests()
    }

    fun setJoinRequests(request: LiveData<List<Request>>){
        requests = request
    }

}