package com.example.aait_hub.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = arrayOf(Like::class, Post::class, Vote::class, Message::class, Request::class, Project::class, Student::class, PostReturn::class), version = 1)
@TypeConverters(DateTypeConverter::class)
abstract class AAIThubDatabase: RoomDatabase() {
    abstract fun likeDao():LikeDao
    abstract fun postDao():PostDao
    abstract fun voteDao():VoteDao
    abstract fun messageDao():MessageDao
    abstract fun requestDao():RequestDao
    abstract fun projectDao():ProjectDao
    abstract fun studentDao():StudentDao
    abstract fun postReturnDao():PostReturnDao

    companion object {

        @Volatile
        private var INSTANCE: AAIThubDatabase? = null

        fun getDatabase(context: Context): AAIThubDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AAIThubDatabase::class.java, "carSpareParts"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}