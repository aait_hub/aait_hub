package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface VoteDao {
    @Query("SELECT * FROM Vote")
    fun getAllVotes(): LiveData<List<Vote>>

    @Query("SELECT * FROM Vote WHERE s_studentId=:studentId")
    fun getVotesByStudentId(studentId:Int): LiveData<List<Vote>>

    @Query("SELECT * FROM Vote WHERE p_postId=:postId")
    fun getVotesByPostId(postId:Int): LiveData<List<Vote>>

    @Query("SELECT * FROM Vote WHERE p_postId=:postId AND s_studentId=:studentId LIMIT 1")
    fun getVoteByPostIdAndStudentId(postId:Int, studentId:Int): LiveData<Vote>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addVote(vote: Vote):Long

    @Delete
    fun removeVote(vote: Vote):Int

    @Query("DELETE FROM Vote WHERE s_studentId=:studentId")
    fun removeVotesByStudentId(studentId:Int):Int

    @Query("DELETE FROM Vote")
    fun removeAllVotes():Int
}