package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "Project")
data class Project(
    @PrimaryKey(autoGenerate = true) val projectId:Int,
    val projectOwnerId : Int,
    val projectName:String,
    val projectDescription:String,
    val projectDate: Date
):Serializable