package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface LikeDao {
    @Query("SELECT * FROM `Like`")
    fun getAllLikes(): LiveData<List<Like>>

    @Query("SELECT * FROM `Like` WHERE p_postId=:postId")
    fun getLikesByPostId(postId:Int): LiveData<List<Like>>

    @Query("SELECT * FROM `Like` WHERE p_postId=:postId AND s_studentId=:studentId LIMIT 1")
    fun getLikeByPostIdAndStudentId(postId:Int, studentId:Int): LiveData<Like>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addLike(like: Like):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAllLike(likes: List<Like>)

    @Update
    fun updateLike(like: Like):Int

    @Delete
    fun removeLike(like: Like):Int

    @Query("DELETE FROM `Like`")
    fun removeAllLikes():Int
}