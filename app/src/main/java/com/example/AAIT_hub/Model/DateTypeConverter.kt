package com.example.aait_hub.model

import androidx.room.TypeConverter
import java.util.*


class DateTypeConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun toDate(dateLong: Long?): Date? {
            return if (dateLong == null) null else Date(dateLong)
        }

        @TypeConverter
        @JvmStatic
        fun fromDate(date: Date?): Long? {
            return (if (date == null) null else date.getTime().toLong())
        }
    }
}