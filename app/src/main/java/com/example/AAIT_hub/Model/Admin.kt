package com.example.aait_hub.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ADMIN")
data class Admin(
    @PrimaryKey(autoGenerate = true) val adminId:Int,
    val firstName:String,
    val lastName:String,
    val userName:String,
    val password:String
)