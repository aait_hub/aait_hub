package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Vote")
data class Vote(
    @PrimaryKey(autoGenerate = true) val voteId:Int,
    @Embedded(prefix = "s_") val student: Student,
    @Embedded(prefix = "p_") val post: Post,
    val voteDate: Date
    //val seen:Boolean
)