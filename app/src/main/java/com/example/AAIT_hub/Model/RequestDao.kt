package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RequestDao {
    @Query("SELECT * FROM Request")
    fun getAllRequests():LiveData<List<Request>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addRequest(request: Request):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAllRequest(request: List<Request>)

    @Delete
    fun removeRequest(request: Request):Int

    @Query("DELETE FROM Request")
    fun removeAllRequests()
}