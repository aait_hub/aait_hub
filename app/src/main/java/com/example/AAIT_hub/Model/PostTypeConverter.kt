package com.example.aait_hub.model

import androidx.room.TypeConverter

class PostTypeConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun toPostType(postTypeInString: String?): PostType? {
            return if(postTypeInString == null) null
            else {
                if(postTypeInString == "AdminPost")
                    PostType.AdminPost
                else if(postTypeInString == "ProjectProgressPost")
                    PostType.ProjectProgressPost
                else
                    PostType.ProjectRequestPost
            }
        }

        @TypeConverter
        @JvmStatic
        fun fromPostType(postType: PostType?): String? {
            return (if (postType == null) null
            else {
                if(postType == PostType.AdminPost)
                    "AdminPost"
                else if(postType == PostType.ProjectProgressPost)
                    "ProjectProgressPost"
                else
                    "ProjectRequestPost"
            })
        }
    }
}