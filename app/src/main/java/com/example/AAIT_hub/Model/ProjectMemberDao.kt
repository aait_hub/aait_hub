package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ProjectMemberDao {
    @Query("SELECT * FROM ProjectMember")
    fun getAllProjectMembers():LiveData<List<ProjectMember>>

    @Query("SELECT * FROM ProjectMember WHERE p_projectId=:projectId")
    fun getProjectMembersByProjectId(projectId:Int):LiveData<List<ProjectMember>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addProjectMember(projectMember: ProjectMember):Long

    @Delete
    fun removeProjectMember(projectMember: ProjectMember)
}