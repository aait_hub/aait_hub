package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "Message")
data class Message(
    @PrimaryKey(autoGenerate = true) val messageId:Int,
    @Embedded(prefix = "p_") val project: Project,
    @Embedded(prefix = "s_") val student: Student,
    val messageContent:String,
    val messageDate: Date
    //val seen:Boolean,
    //val isFile:Boolean
):Serializable