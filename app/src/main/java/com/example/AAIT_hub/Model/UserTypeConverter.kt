package com.example.aait_hub.model

import androidx.room.TypeConverter

class UserTypeConverter {
    companion object{
        @TypeConverter
        @JvmStatic
        fun toUserType(userTypeInString: String?): UserType? {
            return if(userTypeInString == null) null
            else {
                if(userTypeInString == "Admin")
                    UserType.Admin
                else
                    UserType.Student
            }
        }

        @TypeConverter
        @JvmStatic
        fun fromUserType(userType: UserType?): String? {
            return (if (userType == null) null
            else {
                if(userType == UserType.Admin)
                    "Admin"
                else
                    "Student"
            })
        }
    }
}