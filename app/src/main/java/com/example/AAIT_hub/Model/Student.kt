package com.example.aait_hub.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "USER")
data class Student(
    @PrimaryKey(autoGenerate = true) val studentId:Int,
    val firstName:String,
    val lastName:String,
    val userName:String,
    val password:String,
    val bio:String,
    val department:String
)