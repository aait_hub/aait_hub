package com.example.aait_hub.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "Post")
data class Post(
    @PrimaryKey(autoGenerate = true) val postId:Int,
    val postOwnerId:Int,
    val userType:Int,
    var postTitle:String,
    var postContent:String,
    val postImage:String,
    val postDate: Date,
    val postType:Int,
    val isProject:Boolean
):Serializable

/*enum class PostType{
    ADMIN_POST,
    PROGRESS_POST,
    PROJECT_POST
}

enum class UserType{
    ADMIN,
    USER
}*/