package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ProjectDao {

    @Query("SELECT * FROM Project")
    fun getAllProjects(): LiveData<List<Project>>

    @Query("SELECT * FROM Project WHERE projectOwnerId=:ownerId")
    fun getProjectByOwnerId(ownerId:Int):LiveData<List<Project>>

    @Query("SELECT * FROM Project WHERE projectId=:projectId LIMIT 1")
    fun getProjectById(projectId:Int):LiveData<Project>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addProject(project: Project):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllProjects(projects: List<Project>)

    @Update
    fun updateProject(project: Project):Int

    @Delete
    fun removeProject(project: Project):Int

    @Query("DELETE FROM Project")
    fun removeAllProjects():Int
}