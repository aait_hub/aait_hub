package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MessageDao {
    @Query("SELECT * FROM Message")
    fun getAllMessages(): LiveData<List<Message>>

    @Query("SELECT * FROM Message WHERE s_studentId=:studentId")
    fun getMessagesByStudentId(studentId:Int): LiveData<List<Message>>

    @Query("SELECT * FROM Message WHERE p_projectId=:projectId")
    fun getMessagesByProjectId(projectId:Int): LiveData<List<Message>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMessage(message: Message):Long

    @Update
    fun updateMessage(message: Message):Int

    @Delete
    fun removeMessgae(message: Message):Int

    @Query("DELETE FROM Message")
    fun removeAllMessages():Int
}