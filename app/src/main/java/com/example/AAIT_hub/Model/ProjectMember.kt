package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ProjectMember")
data class ProjectMember(
    @PrimaryKey(autoGenerate = true) val projectMemberId:Int,
    @Embedded(prefix = "p_") val project: Project,
    @Embedded(prefix = "s_") val student: Student
)