package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Like")
data class Like(
    @PrimaryKey(autoGenerate = true) val likeId:Int,
    @Embedded(prefix = "s_") val student: Student,
    @Embedded(prefix = "p_") val post:Post,
    val likeDate: Date
)