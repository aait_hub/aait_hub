package com.example.aait_hub.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "Request")
data class Request(
    @PrimaryKey(autoGenerate = true) val id:Int,
    val studentId: Int,
    val projectId: Int,
    var date: Date
    //val seen:Boolean
)