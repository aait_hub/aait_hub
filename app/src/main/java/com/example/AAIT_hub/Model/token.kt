package com.example.aait_hub.model

import java.util.*

data class token (
    val tokenId:Int,
    val userId:Int,
    val userType:Int,
    val tokenString:String,
    val expireDate: Date
    )