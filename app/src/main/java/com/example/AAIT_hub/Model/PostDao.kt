package com.example.aait_hub.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PostDao {
    @Query("SELECT * FROM Post")
    fun getAllPosts(): LiveData<List<Post>>

    @Query("SELECT * FROM Post WHERE postId=:postId LIMIT 1")
    fun getPostByPostId(postId:Int):LiveData<Post>

    @Query("SELECT * FROM Post WHERE postOwnerId=:ownerId")
    fun getPostByOwnerId(ownerId:Int): LiveData<List<Post>>

    @Query("SELECT * FROM Post WHERE postType=:postType")
    fun getPostByPostType(postType:Int): LiveData<List<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPost(post: Post):Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPosts(posts:List<Post>)
    //@Update
    //fun updatePost(post: Post):Int

    //@Delete
    //fun removePost(post: Post):Int

    @Query("DELETE FROM Post")
    fun removeAllPosts():Int
}