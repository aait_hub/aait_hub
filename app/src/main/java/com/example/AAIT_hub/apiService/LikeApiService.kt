package com.example.aait_hub.apiService

import com.example.aait_hub.model.Like
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface LikeApiService {

    @GET("Like")
    fun getAllLikesAsync(): Deferred<Response<List<Like>>>

    @POST("Like")
    fun addLikeAsync(@Body likeT: RequestT): Deferred<Response<Void>>

    @DELETE("Like")
    fun removeLikeAsync(@Query("tokenS") tokenS:String, @Query("postId") postId: Int): Deferred<Response<Void>>
}