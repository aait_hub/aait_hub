package com.example.aait_hub.apiService

import com.example.aait_hub.model.token
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginApiService {

    @POST("Login")
    fun loginAsync(@Body loginData: LoginData): Deferred<Response<token>>

    @DELETE("Login")
    fun logoutAsync(@Query("tokenS") tokenS:String): Deferred<Response<Void>>
}