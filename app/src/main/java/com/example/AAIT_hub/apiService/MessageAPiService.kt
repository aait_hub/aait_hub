package com.example.aait_hub.apiService

import com.example.aait_hub.model.Message
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface MessageAPiService {
    @GET("Message/")
    fun getAllMessagesAsync(@Query("tokenS") tokenString:String): Deferred<Response<List<Message>>>

    @GET("Message/{id}/")
    fun getMessageByProjectIdAsync(@Path("id") id:Int, @Query("tokenS") tokenString:String): Deferred<Response<Message>>

    @POST("Message")
    fun addPostAsync(@Body message: Message): Deferred<Response<Void>>

    @PUT("Message/{id}")
    fun updatePostAsync(@Path("id") id:Int, @Body message: Message): Deferred<Response<Void>>

    @DELETE("Message/{id}")
    fun removePostAsync(@Path("id") id:Int, @Query("tokenS") tokenS: String): Deferred<Response<Void>>
}