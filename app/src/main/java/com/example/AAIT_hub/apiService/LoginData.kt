package com.example.aait_hub.apiService

data class LoginData(
    val userName:String,
    val password:String,
    val userType:Int
)