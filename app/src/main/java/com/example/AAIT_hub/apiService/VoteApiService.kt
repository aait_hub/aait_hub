package com.example.aait_hub.apiService

import com.example.aait_hub.model.Vote
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface VoteApiService {

    @GET("Vote")
    fun getAllVotesAsync(): Deferred<Response<List<Vote>>>

    @POST("Vote")
    fun addVoteAsync(@Body voteT: RequestT): Deferred<Response<Void>>

    @DELETE("Vote")
    fun removeVoteAsync(@Query("tokenS") tokenS:String, @Query("postId") postId: Int): Deferred<Response<Void>>
}