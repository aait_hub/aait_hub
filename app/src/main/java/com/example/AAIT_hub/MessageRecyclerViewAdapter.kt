package com.example.aait_hub

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.databinding.MessageRecyclerViewItemBinding
import com.example.aait_hub.model.*
import kotlinx.android.synthetic.main.fragment_message.view.*
import kotlinx.android.synthetic.main.message_recycler_view_item.view.*
import java.util.*

class MessageRecyclerViewAdapter(private val context: Context): RecyclerView.Adapter<MessageRecyclerViewAdapter.MessageViewHolder>() {
    private val post = Post(1, 1, UserType.Student, "content", "test.png", Date(), PostType.ProjectRequestPost, false)
    private val student = Student(1, "fName","lName","uName", "pass", "bio", "software")
    private val student2 = Student(2, "fName2","lName2","uName2", "pass2", "bio2", "software2")
    private val student3 = Student(3, "fName3","lName3","uName3", "pass", "bio", "software")
    private val student4 = Student(4, "fName4","lName4","uName4", "pass", "bio", "software")
    private val student5 = Student(5, "fName5","lName5","uName5", "pass", "bio", "software")

    val project = Project(1, student, post,"project1", "description 1", Date())
    val project2 = Project(2, student,post ,"project2", "description 2", Date())
    val project3 = Project(3, student, post,"project3", "description 3", Date())

    private var messages: List<Message> = listOf(
        Message(1, project, student, "hi message one hi message one hi message one hi message one hi message one hi message one", Date()),
        Message(2, project2, student2, "hey message two hey message two hey message two hey message two hey message two hey message two", Date()),
        Message(3, project, student3, "selam message three selam message three selam message three selam message three selam message three", Date()),
        Message(4, project3, student4, "pis message four pis message four pis message four pis message four pis message four pis message four", Date()),
        Message(5, project, student5, "hooo message five hooo message five hooo message five hooo message fivehooo message fivehooo message five", Date())
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = MessageRecyclerViewItemBinding.inflate(inflater)

        recyclerViewItem.root.setOnClickListener {
            val recyclerView:RecyclerView = parent.message_recycler_view
            val itemPosition = recyclerView.getChildLayoutPosition(recyclerViewItem.root)
            val message = messages[itemPosition]
            val messageBundle = Bundle()
            messageBundle.putSerializable("message", message)
            it.findNavController().navigate(R.id.detailMessageFragment, messageBundle)
        }

        return MessageViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return messages.count()
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = messages[position]
        holder.bind(message)

    }

    internal fun setMessages(messages:List<Message>){
        this.messages = messages
        notifyDataSetChanged()
    }

    class MessageViewHolder(private val itemBinding: MessageRecyclerViewItemBinding):RecyclerView.ViewHolder(itemBinding.root){
        fun bind(message:Message){
            itemBinding.itemBindingMessage = message
        }
    }
}