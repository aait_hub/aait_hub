package com.example.aait_hub

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.model.*
import java.util.*
import com.example.aait_hub.databinding.RequestRecyclerViewItemBinding

class RequestsRecyclerViewAdapter(private val context: Context): RecyclerView.Adapter<RequestsRecyclerViewAdapter.RequestViewHolder>() {
    private var requests:List<Request> = emptyList()
    private lateinit var listener: OnRequestButtonClickManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = RequestRecyclerViewItemBinding.inflate(inflater)

        if (context is OnRequestButtonClickManager){
            listener = context
        }
        return RequestViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return requests.size
    }

    override fun onBindViewHolder(holder: RequestViewHolder, position: Int) {
        val request = requests[position]
        holder.bind(request, listener)
    }

    internal fun setRequests(requests:List<Request>){
        this.requests = requests
        notifyDataSetChanged()
    }

    class RequestViewHolder(private val itemBinding:RequestRecyclerViewItemBinding):RecyclerView.ViewHolder(itemBinding.root){
        fun bind(request: Request, listener: OnRequestButtonClickManager){
            itemBinding.itemBindingStudentName = "student Name"
            itemBinding.requestClickHandler = RequestFragClickHandler(request, listener)
        }
    }
    interface OnRequestButtonClickManager{
        fun onShowRequestAcceptButtonClick(student:Student, project: Project)
        fun onShowRequestDeclineButtonClick(requestId: Int)
    }

    class RequestFragClickHandler(val request: Request, private val listener: OnRequestButtonClickManager){
        fun onAcceptClick(view:View){
            //listener.onShowRequestAcceptButtonClick(request.id)
        }
        fun onDeclineClick(view: View){
            listener.onShowRequestDeclineButtonClick(request.projectId)
        }
    }
}