package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.viewModel.PostViewModel
import kotlinx.android.synthetic.main.fragment_show_posts.view.*
import androidx.lifecycle.Observer
import com.google.android.material.floatingactionbutton.FloatingActionButton


class ShowPostsFragment : Fragment() {
    private lateinit var showPostRecyclerView: RecyclerView
    private lateinit var context1:Context
    private lateinit var postViewModel: PostViewModel
    private lateinit var postRecyclerViewAdapter: PostRecyclerViewAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context1 = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postViewModel = activity?.run {
            ViewModelProviders.of(this).get(PostViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        postRecyclerViewAdapter = PostRecyclerViewAdapter(context1)

        /*val postsFromAPI = postViewModel.getAllPostsAsync()
        if(postsFromAPI != null){
            for(post:Post in postsFromAPI){
                postViewModel.addPost(post)
            }
        }*/

        postViewModel.postReturns.observe(this, Observer {
                posts -> posts?.let {postRecyclerViewAdapter.setPosts(posts.reversed())}
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_show_posts, container, false)

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).show()

        showPostRecyclerView = view.ShowPostRecyclerView
        showPostRecyclerView.layoutManager = LinearLayoutManager(activity)
        showPostRecyclerView.adapter = postRecyclerViewAdapter
        showPostRecyclerView.setHasFixedSize(true)

        postViewModel.setPostReturn(postViewModel.getAllPosts())

        return view
    }
}
