package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.aait_hub.apiService.LoginData
import com.example.aait_hub.databinding.FragmentLoginBinding
import com.example.aait_hub.utils.TokenHelper
import com.example.aait_hub.viewModel.LoginViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class LoginFragment : Fragment() {

    private lateinit var loginViewModel:LoginViewModel
    private lateinit var drawerLayout:DrawerLayout
    private lateinit var toolBar:Toolbar
    private lateinit var fab:FloatingActionButton
    private lateinit var _context:Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        _context = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = activity?.run {
            ViewModelProviders.of(this).get(LoginViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val mainactivity:MainActivity = activity as MainActivity

        drawerLayout = mainactivity.findViewById(R.id.drawer_layout)
        fab = mainactivity.findViewById(R.id.fab)
        toolBar = mainactivity.findViewById(R.id.toolbar)

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        fab.hide()
        toolBar.visibility = View.GONE

        val binding: FragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        val view = binding.root

        binding.loginClickHandler = LoginClickHandler()
        return view
    }

    inner class LoginClickHandler{
        fun login(userName:String, password:String){//userType: UserType

            when (val token = loginViewModel.loginAsync(LoginData(userName, password, 1))) {
                null -> {
                    Log.d("LoginClickHandler", "login failed")
                }
                else -> {

                    Log.d("LoginClickHandler", "login succ")
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED)
                    fab.show()
                    toolBar.visibility = View.VISIBLE

                    TokenHelper.saveTokenToSharedPreference(context, token.tokenId, token.userId, token.userType, token.tokenString, token.expireDate)
                    val navController = findNavController()
                    navController.navigate(R.id.action_loginFragment_to_showPostsFragment)
                }
            }
        }
    }
}
