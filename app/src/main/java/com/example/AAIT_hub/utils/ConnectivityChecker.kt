package com.example.aait_hub.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class ConnectivityChecker(private val context: Context) {
        fun isConnectedToNetwork():Boolean{
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo:NetworkInfo? = connectivityManager.activeNetworkInfo

            return networkInfo != null && networkInfo.isConnected
    }
}