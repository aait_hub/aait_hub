package com.example.aait_hub.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.aait_hub.*
import com.example.aait_hub.model.token
import java.util.*

class TokenHelper {
    companion object{
        fun getTokenFromSharedPreference(context: Context): token {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(SHAREDPREFERENCEKEY, Context.MODE_PRIVATE)
            val userIdFromSP = sharedPreference.getInt(USERIDKEY, -1)
            val userTypeFromSP = sharedPreference.getInt(USERTYPEKEY, -1)
            val tokenStringFromSP = sharedPreference.getString(TOKENSTRINGKEY, "")
            val tokenIdFromSP = sharedPreference.getInt(TOKENID, -1)
            //val expireDateFromSP = DateTypeConverter.toDate(sharedPreference.getString(EXPIREDATE, ""))

            return token(tokenIdFromSP, userIdFromSP, userTypeFromSP, tokenStringFromSP, Date())
        }

        fun saveTokenToSharedPreference(context: Context?, tokenId:Int, userId:Int, userType: Int, tokenString: String, expireDate:Date){
            if(context != null){
                val sharedPreference: SharedPreferences = context.getSharedPreferences(SHAREDPREFERENCEKEY, Context.MODE_PRIVATE)

                with(sharedPreference.edit()){
                    putInt(TOKENID, tokenId)
                    putInt(USERIDKEY, userId)
                    putInt(USERTYPEKEY, userType)
                    putString(TOKENSTRINGKEY, tokenString)
                    putString(EXPIREDATE, expireDate.toString())
                    commit()
                }
            }
        }

        fun removeTokenFromSharedPreference(context: Context?)
        {
            if(context != null)
            {
                val sharedPreference: SharedPreferences = context.getSharedPreferences(SHAREDPREFERENCEKEY, Context.MODE_PRIVATE)
                with(sharedPreference.edit()){
                    putInt(TOKENID, -1)
                    putInt(USERIDKEY, -1)
                    putInt(USERTYPEKEY, -1)
                    putString(TOKENSTRINGKEY, "")
                    putString(EXPIREDATE, "")
                    commit()
                }
            }
        }
    }
}