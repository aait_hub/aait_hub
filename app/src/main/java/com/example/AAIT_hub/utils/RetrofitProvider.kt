package com.example.aait_hub.utils

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitProvider {
    companion object{
        fun getRetrofit():Retrofit{
            val client = OkHttpClient
                .Builder()
                .build()

            val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm")
                .create()

            return Retrofit.Builder()
                .baseUrl("http://192.168.43.86:51807/api/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }
    }
}