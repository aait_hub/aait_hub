package com.example.aait_hub.utils

import com.example.aait_hub.model.*

class Utilities {

    companion object{
        fun validatePost(post: Post):Boolean{
            if(post.postId <= 0)
                return false
            else if(post.postOwnerId <= 0)
                return false
            else if(post.postType < 0 || post.postType > 2)
                return false
            else return !(post.userType < 0 || post.userType > 1)
        }

        fun validatePostContent(content:String):Boolean{
            if (content.length > 100){
                return false
            }
            else if(content.isBlank()){
                return false
            }
            else if (content.isNullOrEmpty()){
                return false
            }
            else if (content.isEmpty()){
                return false
            }

            return true
        }

        fun validatePostTitle(content:String):Boolean{
            if (content.length > 50){
                return false
            }
            else if(content.isBlank()){
                return false
            }
            else if (content.isNullOrEmpty()){
                return false
            }
            else if (content.isEmpty()){
                return false
            }

            return true
        }

        fun validateProject(project: Project):Boolean{
            if(project.projectId <= 0){
                return false
            }
            else return project.projectOwnerId > 0
        }

        fun validateProjectName(project: Project):Boolean{
            if(project.projectName.length > 50)
                return false
            else if(project.projectName.isBlank())
                return false
            else if(project.projectName.isNullOrEmpty())
                return false
            else if(project.projectName.isEmpty())
                return false
            return true
        }

        fun validateProjectDescription(project: Project):Boolean{
            if(project.projectDescription.length > 100)
                return false
            else if(project.projectDescription.isBlank())
                return false
            else if(project.projectDescription.isEmpty())
                return false
            else if(project.projectDescription.isNullOrEmpty())
                return false
            return true
        }

        fun validateRequest(request: Request):Boolean{
            if(request.id <= 0){
                return false
            }
            else if(request.projectId <= 0)
                return false
            else return request.studentId > 0
        }

        fun validateVote(vote: Vote):Boolean{
            if(vote.voteId <= 0){
                return false
            }
            else if(vote.post.postId <= 0)
                return false
            else return vote.student.studentId > 0
        }

        fun validateLike(like: Like):Boolean{
            if(like.likeId <= 0){
                return false
            }
            else if(like.post.postId <= 0)
                return false
            else return like.student.studentId > 0
        }
    }
}