package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.databinding.FragmentDetailMessageBinding
import com.example.aait_hub.model.*
import com.example.aait_hub.viewModel.MessageViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_detail_message.view.*


class DetailMessageFragment : Fragment() {
    private lateinit var messageRecyclerView: RecyclerView
    private lateinit var context1: Context
    private lateinit var messageViewModel: MessageViewModel
    private lateinit var detailMessageRecyclerViewAdapter: DetailMessageRecyclerViewAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context1 = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).hide()

        messageViewModel = activity?.run {
            ViewModelProviders.of(this).get(MessageViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        detailMessageRecyclerViewAdapter = DetailMessageRecyclerViewAdapter(context1)

        messageViewModel.detailMessages.observe(this, Observer {
                messages -> messages?.let {detailMessageRecyclerViewAdapter.setMessages(messages.reversed())}
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        var binding: FragmentDetailMessageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_message, container, false)
        var view = binding.root

        messageRecyclerView = view.detail_message_recycler_view
        messageRecyclerView.layoutManager = LinearLayoutManager(activity)
        messageRecyclerView.adapter = detailMessageRecyclerViewAdapter
        messageRecyclerView.setHasFixedSize(true)

        val project = arguments?.getSerializable("project") as Project
        var messages = messageViewModel.getMessagesByProjectId(project.projectId).value

        detailMessageRecyclerViewAdapter.setMessages(messages)

        binding.project = project
        binding.messageViewModel = messageViewModel

        return view
    }
}
