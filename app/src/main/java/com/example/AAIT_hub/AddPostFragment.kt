package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.aait_hub.apiService.PostT
import com.example.aait_hub.databinding.FragmentAddPostBinding
import com.example.aait_hub.model.Post
import com.example.aait_hub.utils.TokenHelper
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*

class AddPostFragment : Fragment() {

    private lateinit var listener:OnAddPostsButtonsClickManager
    private var postType: Int = 2
    private lateinit var _context:Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnAddPostsButtonsClickManager){
            listener = context
            _context = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).hide()

        var binding: FragmentAddPostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_post, container, false)
        var view = binding.root

        binding.addPostClickHandler = AddPostClickHandler()

        return view
    }

    private fun readFields(content: String,postType: Int, postTitle: String):PostT{
        val token = TokenHelper.getTokenFromSharedPreference(_context)
        return PostT(token,Post(0,token.userId, token.userType, postTitle, content, "test.png", Date(), postType, false))
    }

    interface OnAddPostsButtonsClickManager{
        fun onAddPostButtonClicked(postT: PostT)
    }

    inner class AddPostClickHandler{
        fun addPost(content:String, postTitle:String){
            val postT = readFields(content,postType, postTitle)
            listener.onAddPostButtonClicked(postT)
        }

        fun onRadioButtonClick(postType1: Int){
            postType = postType1
        }
    }
}
