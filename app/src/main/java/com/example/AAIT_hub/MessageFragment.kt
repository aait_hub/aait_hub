package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.viewModel.MessageViewModel
import kotlinx.android.synthetic.main.fragment_message.view.*

class MessageFragment : Fragment() {
    private lateinit var messageRecyclerView: RecyclerView
    private lateinit var context1: Context
    private lateinit var messageViewModel: MessageViewModel
    private lateinit var messageRecyclerViewAdapter: MessageRecyclerViewAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context1 = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        messageViewModel = activity?.run {
            ViewModelProviders.of(this).get(MessageViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        messageRecyclerViewAdapter = MessageRecyclerViewAdapter(context1)

        /*messageViewModel.messages.observe(this, Observer {
                messages -> messages?.let {messageRecyclerViewAdapter.setMessages(messages.reversed())}
        })*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_message, container, false)
        messageRecyclerView = view.message_recycler_view
        messageRecyclerView.layoutManager = LinearLayoutManager(activity)
        messageRecyclerView.adapter = messageRecyclerViewAdapter
        messageRecyclerView.setHasFixedSize(true)

        return view
    }


}
