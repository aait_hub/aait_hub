package com.example.aait_hub

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.model.Post
import com.example.aait_hub.databinding.PostRecyclerViewItemBinding
import com.example.aait_hub.model.PostReturn
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_show_posts.view.*
import kotlinx.android.synthetic.main.post_recycler_view_item.view.*

class PostRecyclerViewAdapter(private val context: Context): RecyclerView.Adapter<PostRecyclerViewAdapter.PostViewHolder>() {

    private lateinit var listener:OnAdapterButtonsClickManager
    private var postReturns: List<PostReturn> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = PostRecyclerViewItemBinding.inflate(inflater)

        recyclerViewItem.root.setOnClickListener {
            val recyclerView:RecyclerView = parent.ShowPostRecyclerView
            val itemPosition = recyclerView.getChildLayoutPosition(recyclerViewItem.root)
            val postReturn = postReturns[itemPosition]
            val postBundle = Bundle()
            postBundle.putSerializable("postReturn", postReturn)
            it.findNavController().navigate(R.id.action_showPostsFragment_to_detailPostFragment, postBundle)
        }

        if (context is OnAdapterButtonsClickManager){
            listener = context
        }
        return PostViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return postReturns.count()
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val postReturn = postReturns[position]
        //var postReturnT = PostReturn(1, postReturn, 0, 1, true, false, false)
        holder.bind(postReturn,listener)
        Picasso.with(holder.itemView.context).load(postReturn.post.postImage)
            .error(R.drawable.placeholder)
            .placeholder(R.drawable.placeholder)
            .into(holder.itemView.postImageView)
    }

    internal fun setPosts(postReturns:List<PostReturn>){
        this.postReturns = postReturns
        notifyDataSetChanged()
    }

    class PostViewHolder(private val itemBinding: PostRecyclerViewItemBinding):RecyclerView.ViewHolder(itemBinding.root){
        fun bind(postReturn: PostReturn,listener: OnAdapterButtonsClickManager){
            itemBinding.postReturnBindingItem = postReturn
            itemBinding.postClickHandler = PostClickHandler(postReturn.post, listener)
            itemBinding.isProjectRequestPost = postReturn.post.postType == 2
        }
    }

    interface OnAdapterButtonsClickManager{
        fun onLikeButtonClicked(postId:Int, iLiked: Boolean)
        fun onVoteButtonClicked(postId:Int, iVoted: Boolean)
        fun onJoinButtonClicked(postId: Int, iJoined: Boolean)
        fun onEditButtonClicked(post: Post)
        fun onDeleteButtonClicked(postId: Int)
        fun onSaveButtonClicked(post: Post)
    }

    class PostClickHandler(val post:Post, private val listener: OnAdapterButtonsClickManager){
        fun onLikeClick(iLiked: Boolean){
            listener.onLikeButtonClicked(post.postId, iLiked)
        }
        fun onVoteClick(iVoted:Boolean){
            listener.onVoteButtonClicked(post.postId, iVoted)
        }
        fun onJoinClick(iJoined:Boolean){
            listener.onJoinButtonClicked(post.postId, iJoined)
        }
        fun onEditClick(){
            listener.onEditButtonClicked(post)
        }
        fun onDeleteClick(){
            listener.onDeleteButtonClicked(post.postId)
        }
    }
}