package com.example.aait_hub

import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import com.example.aait_hub.model.*
import com.example.aait_hub.viewModel.PostViewModel
import com.example.aait_hub.viewModel.RequestViewModel
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.aait_hub.apiService.PostT
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.utils.TokenHelper
import kotlinx.android.synthetic.main.activity_main.view.*

const val SHAREDPREFERENCEKEY = "shared_preference_key"
const val USERIDKEY = "user_id_key"
const val USERTYPEKEY = "user_type_key"
const val TOKENSTRINGKEY = "token_string_key"
const val TOKENID = "token_id_key"
const val EXPIREDATE = "expire_date_key"

class MainActivity : AppCompatActivity(),
    AddPostFragment.OnAddPostsButtonsClickManager,
    PostRecyclerViewAdapter.OnAdapterButtonsClickManager,
    RequestsRecyclerViewAdapter.OnRequestButtonClickManager{

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var postViewModel: PostViewModel
    private lateinit var requestViewModel: RequestViewModel
    private lateinit var navController:NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModelProviders = ViewModelProviders.of(this)
        postViewModel = viewModelProviders.get(PostViewModel::class.java)
        requestViewModel = viewModelProviders.get(RequestViewModel::class.java)

        val sharedPreference: SharedPreferences = getSharedPreferences(SHAREDPREFERENCEKEY, Context.MODE_PRIVATE)
        val userIdFromSP = sharedPreference.getInt(USERIDKEY, -1)
        val userTypeFromSP = sharedPreference.getInt(USERTYPEKEY, -1)
        val tokenStringFromSP = sharedPreference.getString(TOKENSTRINGKEY, "")

        setContentView(R.layout.activity_main)
        navController = findNavController(R.id.nav_host_fragment)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            navController.navigate(R.id.addPostFragment)
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        drawerLayout.nav_view.setupWithNavController(navController)

        if (userIdFromSP == -1 || userTypeFromSP == -1 || tokenStringFromSP == "" || tokenStringFromSP == null) {
            navController.navigate(R.id.action_showPostsFragment_to_loginFragment)
        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            var currentFragmentId = navController.currentDestination?.id
            if(currentFragmentId != R.id.loginFragment){
                super.onBackPressed()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp() =
        findNavController(this, R.id.nav_host_fragment).navigateUp()

    override fun onAddPostButtonClicked(postT: PostT) {
        AsyncTask.execute {
            postViewModel.addPost(postT)
        }

        navController.navigate(R.id.action_addPostFragment_to_showPostsFragment)
    }

    //user id will be stored in sharedPreference when user log in and we can get it from there
    override fun onLikeButtonClicked(postId: Int, iLiked:Boolean) {
        AsyncTask.execute {
            val token = TokenHelper.getTokenFromSharedPreference(this)
            val likeT = RequestT(token, postId)
            if (iLiked) {
                postViewModel.removeLike(likeT)

            } else {
                postViewModel.addLike(likeT)
            }
        }
    }

    override fun onVoteButtonClicked(postId: Int, iVoted:Boolean) {

        AsyncTask.execute {
            val token = TokenHelper.getTokenFromSharedPreference(this)
            val voteT = RequestT(token, postId)
            if (iVoted) {
                postViewModel.removeVote(voteT)

            } else {
                postViewModel.addVote(voteT)
            }
        }
    }

    override fun onJoinButtonClicked(postId: Int, iJoined:Boolean) {
        AsyncTask.execute {
            val token = TokenHelper.getTokenFromSharedPreference(this)
            val joinT = RequestT(token, postId)
            if (!iJoined) {
                requestViewModel.addRequest(joinT)

            } else {
                requestViewModel.removeRequest(joinT)
            }
        }
    }

    override fun onEditButtonClicked(post: Post) {
        val postBundle = Bundle()
        postBundle.putSerializable("post", post)
        navController.navigate(R.id.action_showMyPostFragment_to_editPostFragment, postBundle)
    }

    override fun onDeleteButtonClicked(postId: Int) {
        postViewModel.removePost(postId)
        Toast.makeText(this, "Successfully deleted post", Toast.LENGTH_SHORT).show()
    }

    override fun onSaveButtonClicked(post: Post) {
        postViewModel.updatePost(post)
        Toast.makeText(this, "Successfully updated post", Toast.LENGTH_SHORT).show()
    }
    override fun onShowRequestAcceptButtonClick(student: Student, project: Project) {

    }

    override fun onShowRequestDeclineButtonClick(requestId: Int) {
        val token = TokenHelper.getTokenFromSharedPreference(this)
        requestViewModel.removeRequest(RequestT(token, requestId))
    }
}
