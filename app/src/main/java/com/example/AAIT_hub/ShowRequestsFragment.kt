package com.example.aait_hub


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aait_hub.viewModel.RequestViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_show_request.view.*


class ShowRequestsFragment : Fragment() {
    private lateinit var showRequestRecyclerView: RecyclerView
    private lateinit var context1:Context
    private lateinit var requestViewModel: RequestViewModel
    private lateinit var requestsRecyclerViewAdapter: RequestsRecyclerViewAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        context1 = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestViewModel = activity?.run {
            ViewModelProviders.of(this).get(RequestViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        requestsRecyclerViewAdapter = RequestsRecyclerViewAdapter(context1)

        requestViewModel.requests.observe(this, Observer {
                requests -> requests?.let {requestsRecyclerViewAdapter.setRequests(requests.reversed())}
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_show_request, container, false)

        val mainactivity:MainActivity = activity as MainActivity
        mainactivity.findViewById<FloatingActionButton>(R.id.fab).show()

        showRequestRecyclerView = view.show_requests_recycler_view
        showRequestRecyclerView.layoutManager = LinearLayoutManager(activity)
        showRequestRecyclerView.adapter = requestsRecyclerViewAdapter
        showRequestRecyclerView.setHasFixedSize(true)

        requestViewModel.setJoinRequests(requestViewModel.getAllRequests())
        return view
    }

}
