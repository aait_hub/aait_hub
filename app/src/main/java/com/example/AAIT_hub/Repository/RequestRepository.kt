package com.example.aait_hub.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.aait_hub.apiService.PostApiService
import com.example.aait_hub.apiService.RequestApiService
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.model.Request
import com.example.aait_hub.model.RequestDao
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import com.example.aait_hub.utils.TokenHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import java.util.*

class RequestRepository(private val requestDao: RequestDao, private val context: Context) {

    private var requestApiService: RequestApiService = RetrofitProvider.getRetrofit().create(RequestApiService::class.java)
    private var connectivityChecker: ConnectivityChecker = ConnectivityChecker(context)

    fun getAllRequests(): LiveData<List<Request>>{
        if(connectivityChecker.isConnectedToNetwork()){
            var request:List<Request>?

            GlobalScope.launch(Dispatchers.IO) {
                val response: Response<List<Request>> = requestApiService.getAllRequestAsync(TokenHelper.getTokenFromSharedPreference(context).tokenString).await()
                if(response.isSuccessful) {
                    request = response.body()
                    if(request != null){
                        /*val req:MutableList<Request> = emptyList<Request>().toMutableList()
                        for(r:Request in request as List<Request>){
                            r.date = Date()
                            req.add(r)
                        } */
                        requestDao.removeAllRequests()
                        requestDao.addAllRequest(request as List<Request>)
                        Log.d("GetProjectAsync", "project not null")
                    }
                    else Log.d("GetProjectAsync", "project null")
                }
                else Log.d("GetProjectAsync", response.message())
            }
        }
        return requestDao.getAllRequests()
    }

    suspend fun addRequest(request: RequestT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()){
            var isSuccessful = false

            val response: Response<Void> = requestApiService.addRequestAsync(request.postId, request.token).await()
            if(response.isSuccessful)
                isSuccessful = true
            else Log.d("Add request ASync", response.message())

            return isSuccessful
        }
        return false
    }

    suspend fun removeRequest(request: RequestT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()){
            var isSuccessful = false
            val response: Response<Void> = requestApiService.removeRequestAsync(request.postId, request.token.tokenString).await()
            if(response.isSuccessful)
                isSuccessful = true
            else Log.d("Remove request ASync", response.code().toString())

            return isSuccessful
        }
        return false
    }

}