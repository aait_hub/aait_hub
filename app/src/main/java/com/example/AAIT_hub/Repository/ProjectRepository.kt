package com.example.aait_hub.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.aait_hub.apiService.ProjectApiService
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.model.Project
import com.example.aait_hub.model.ProjectDao
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import com.example.aait_hub.utils.TokenHelper
import kotlinx.coroutines.*
import retrofit2.Response

class ProjectRepository(private val context: Context, private val projectDao: ProjectDao) {
    private var projectApiService:ProjectApiService = RetrofitProvider.getRetrofit().create(ProjectApiService::class.java)
    private var connectivityChecker: ConnectivityChecker = ConnectivityChecker(context)

    fun getAllProjects(): LiveData<List<Project>>{
        //return projectDao.getAllProjects()
        GlobalScope.launch { refreshProjects() }
        return projectDao.getAllProjects()
    }

    fun getProjectById(projectId:Int): LiveData<Project>{
        //return projectDao.getAllProjects()
        GlobalScope.launch { refreshProjects() }
        return getProjectByProjectIdAsync(projectId)
    }

    fun addProject(postId: Int):Boolean{
        if(connectivityChecker.isConnectedToNetwork()){
            val isSuccessFul = addProjectAsync(postId)
            GlobalScope.launch { refreshProjects() }
            return isSuccessFul
        }
        return false
    }

    fun updateProject(project: Project):Int{
        return projectDao.updateProject(project)
    }

    fun removeAllProjects():Int{
        GlobalScope.launch(Dispatchers.IO) { projectDao.removeAllProjects() }
        return 1
    }

    suspend fun refreshProjects(){
        withContext(Dispatchers.IO) {
            if(connectivityChecker.isConnectedToNetwork()){
                val postReturns = getProjectsAsync().value
                if(postReturns != null) {
                    removeAllProjects()
                    projectDao.insertAllProjects(postReturns)
                }
            }
            //else notConnected()
        }
    }
    private fun notConnected(){
        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
    }

    //API SERVICES
    fun getProjectsAsync():LiveData<List<Project>>{
        if(connectivityChecker.isConnectedToNetwork()){
            var projects:List<Project>? = null

            GlobalScope.launch(Dispatchers.IO) {
                val response: Response<List<Project>> = projectApiService.getProjectsByStudentIdAsync(TokenHelper.getTokenFromSharedPreference(context).tokenString).await()
                if(response.isSuccessful) {
                    projects = response.body()
                    if(projects != null){
                        projectDao.removeAllProjects()
                        projectDao.insertAllProjects(projects as List<Project>)
                        Log.d("GetProjectAsync", "project not null")
                    }
                    else Log.d("GetProjectAsync", "project null")
                }
                else Log.d("GetProjectAsync", response.message())
            }
        }
        return projectDao.getAllProjects()
    }

    fun addProjectAsync(postId: Int):Boolean{
        var isSuccessful = false

        if(connectivityChecker.isConnectedToNetwork()){
            runBlocking(Dispatchers.IO) {
                val response:Response<Void> = projectApiService.addProjectAsync(RequestT(TokenHelper.getTokenFromSharedPreference(context), postId)).await()
                if(response.isSuccessful)
                    isSuccessful = true
                else Log.d("Add project ASync", response.code().toString())
            }
        }
        return isSuccessful
    }

    fun getProjectByProjectIdAsync(id:Int): LiveData<Project>{
        var project: Project? = null
        if(connectivityChecker.isConnectedToNetwork()){
            runBlocking(Dispatchers.IO) {
                val response:Response<Project> = projectApiService.getProjectsByStudentIdAndProjectIdAsync(id, TokenHelper.getTokenFromSharedPreference(context).tokenString).await()

                if(response.isSuccessful)
                    project = response.body()
            }
        }

        return MutableLiveData<Project>(project)
    }
}