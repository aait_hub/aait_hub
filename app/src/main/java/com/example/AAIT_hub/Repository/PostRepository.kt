package com.example.aait_hub.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.aait_hub.apiService.PostApiService
import com.example.aait_hub.apiService.PostT
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.model.*
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import com.example.aait_hub.utils.TokenHelper
import kotlinx.coroutines.*
import retrofit2.Response

class PostRepository(private val context: Context,private val postDao: PostDao, private val postReturnDao: PostReturnDao) {
    private var postApiService:PostApiService = RetrofitProvider.getRetrofit().create(PostApiService::class.java)
    private var connectivityChecker:ConnectivityChecker = ConnectivityChecker(context)

    fun getPostByOwnerId(ownerId:Int): LiveData<List<PostReturn>>{
        return postReturnDao.getAllMyPostReturns(ownerId)
    }

    private fun removeAllPosts():Int{
        return postReturnDao.removeAllPostReturns()
    }

    suspend fun refreshPost(){
        if(connectivityChecker.isConnectedToNetwork())
        {
            withContext(Dispatchers.IO) {
                val postReturns = getAllPostsAsync().value
                if(postReturns != null) {
                    removeAllPosts()
                    postReturnDao.insertAllPostReturns(postReturns)
                }
            }
        }
    }

    private fun notConnected(){
        Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show()
    }

    //API SERVICES
    fun getAllPostsAsync():LiveData<List<PostReturn>>{
        if(connectivityChecker.isConnectedToNetwork()) {
            var postsReturn: List<PostReturn>?
            val postReturns: MutableList<PostReturn> = emptyList<PostReturn>().toMutableList()

            GlobalScope.launch(Dispatchers.IO) {
                val response: Response<List<PostReturn>> =
                    postApiService.getAllPostsAsync(TokenHelper.getTokenFromSharedPreference(context).tokenString)
                        .await()
                if (response.isSuccessful) {
                    postsReturn = response.body()

                    if (postsReturn != null) {
                        var count = 1
                        for (postReturn: PostReturn in postsReturn as List<PostReturn>) {
                            postReturn.id = count
                            postReturns.add(postReturn)
                            count++
                        }
                        removeAllPosts()
                        postReturnDao.insertAllPostReturns(postReturns)
                    }
                }
            }
        }
        else notConnected()
        return postReturnDao.getAllPostReturns()
    }

    suspend fun addPostAsync(postT: PostT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()) {
            var isSuccessful = false
            val response:Response<Void> = postApiService.addPostAsync(postT).await()
            if(response.isSuccessful)
                isSuccessful = true
            else Log.d("Add post ASync", response.code().toString())
            return isSuccessful
        } else notConnected()
        return false
    }

    suspend fun updatePostAsync(postT: PostT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()) {
            var isSuccessful = false
            val response:Response<Void> = postApiService.updatePostAsync(postT.post.postId, postT).await()
            if(response.isSuccessful)
                isSuccessful = true
            else Log.d("Update post ASync", response.code().toString())
            return isSuccessful
        } else notConnected()
        return false
    }

    suspend fun removePostAsync(postId: Int, token: token):Boolean{
        if(connectivityChecker.isConnectedToNetwork()) {
            var isSuccessful = false
            Log.d("Remove post token id", token.tokenString + " " + postId.toString())
            val response:Response<Void> = postApiService.removePostAsync(postId, token.tokenString).await()
            if(response.isSuccessful)
                isSuccessful = true
            else Log.d("Remove post ASync", response.code().toString())
            return isSuccessful
        } else notConnected()
        return false
    }

    private fun getPostByPostIdAsync(id:Int): LiveData<Post>{
        var postReturn: PostReturn? = null
        if(connectivityChecker.isConnectedToNetwork()){
            GlobalScope.launch(Dispatchers.IO) {
                val response:Response<PostReturn> = postApiService.getPostByPostIdAsync(id, TokenHelper.getTokenFromSharedPreference(context).tokenString).await()

                if(response.isSuccessful)
                    postReturn = response.body()
            }
        }
        else notConnected()
        return MutableLiveData<Post>(postReturn?.post)
    }
}