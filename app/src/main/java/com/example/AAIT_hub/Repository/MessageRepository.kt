package com.example.aait_hub.repository

import androidx.lifecycle.LiveData
import com.example.aait_hub.model.Message
import com.example.aait_hub.model.MessageDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MessageRepository(private val messageDao: MessageDao) {

    fun getAllMessages(): LiveData<List<Message>> {
        return messageDao.getAllMessages()
    }

    fun getMessagesByStudentId(studentId:Int): LiveData<List<Message>>{
        return messageDao.getMessagesByStudentId(studentId)
    }

    fun getMessagesByProjectId(projectId:Int): LiveData<List<Message>>{
        return messageDao.getMessagesByProjectId(projectId)
    }

    fun addMessage(message: Message):Long{
        return messageDao.addMessage(message)
    }

    fun updateMessage(message: Message):Int{
        return messageDao.updateMessage(message)
    }

    fun removeMessgae(message: Message):Int{
        return messageDao.removeMessgae(message)
    }

    fun removeAllMessages(){
        GlobalScope.launch(Dispatchers.IO) { messageDao.removeAllMessages() }
    }
}