package com.example.aait_hub.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.aait_hub.apiService.LikeApiService
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.model.Like
import com.example.aait_hub.model.LikeDao
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class LikeRepository(private val context: Context, private val likeDao: LikeDao) {
    private var likeApiService: LikeApiService = RetrofitProvider.getRetrofit().create(LikeApiService::class.java)
    private var connectivityChecker: ConnectivityChecker = ConnectivityChecker(context)

    suspend fun addLike(requestT: RequestT):Boolean
    {
        if(connectivityChecker.isConnectedToNetwork()){
            return addLikeAsync(requestT)
        }
        return false
    }

    suspend fun removeLike(requestT: RequestT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()){
            return removeLikeAsync(requestT)
        }
        return false
    }

    //API Service

    private suspend fun addLikeAsync(likeT: RequestT): Boolean{
        var isSuccessful = false

        val response = likeApiService.addLikeAsync(likeT).await()

        if(response.isSuccessful)
            isSuccessful = true

        return isSuccessful
    }

    private suspend fun removeLikeAsync(likeT: RequestT): Boolean{
        var isSuccessful = false

        val response = likeApiService.removeLikeAsync(likeT.token.tokenString, likeT.postId).await()

        if(response.isSuccessful)
            isSuccessful = true

        return isSuccessful
    }
}