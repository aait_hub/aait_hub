package com.example.aait_hub.repository

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.aait_hub.apiService.RequestT
import com.example.aait_hub.apiService.VoteApiService
import com.example.aait_hub.model.Vote
import com.example.aait_hub.model.VoteDao
import com.example.aait_hub.utils.ConnectivityChecker
import com.example.aait_hub.utils.RetrofitProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class VoteRepository(private val context: Context, private val voteDao: VoteDao) {
    private var voteApiService:VoteApiService = RetrofitProvider.getRetrofit().create(VoteApiService::class.java)
    private var connectivityChecker: ConnectivityChecker = ConnectivityChecker(context)

    suspend fun addVote(voteT: RequestT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()){
            return addVoteAsync(voteT)
        }
        return false
    }

    suspend fun removeVote(voteT: RequestT):Boolean{
        if(connectivityChecker.isConnectedToNetwork()){
            return removeVoteAsync(voteT)
        }
        return false
    }

    //API Service
    private suspend fun addVoteAsync(voteT:RequestT): Boolean{
        var isSuccessful = false

        val response = voteApiService.addVoteAsync(voteT).await()

        if(response.isSuccessful)
            isSuccessful = true

        return isSuccessful
    }

    private suspend fun removeVoteAsync(voteT:RequestT): Boolean{
        var isSuccessful = false

        val response = voteApiService.removeVoteAsync(voteT.token.tokenString, voteT.postId).await()

        if(response.isSuccessful)
            isSuccessful = true

        return isSuccessful
    }
}