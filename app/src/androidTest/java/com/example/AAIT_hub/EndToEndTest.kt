package com.example.aait_hub

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class EndToEndTest {
    @Test
    fun testLogin(){
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        //enter name
        onView(withId(R.id.userNameEditText)).perform(typeText("robi"))

        //enter password
        onView(withId(R.id.PasswordEditText)).perform(typeText("robi"))

        //click login button
        onView(withId(R.id.loginButton)).perform(click())

        //check if show post fragment is opened
        onView(withId(R.id.ShowPostRecyclerView)).check(matches(isDisplayed()))

    }

    @Test
    fun testAddPost(){
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        //enter name
        onView(withId(R.id.userNameEditText)).perform(typeText("robi"))

        //enter password
        onView(withId(R.id.PasswordEditText)).perform(typeText("robi"))

        //click login button
        onView(withId(R.id.loginButton)).perform(click())

        //click add post(navigate to add post screen)
        onView(withId(R.id.showPostsFragment)).perform(click())

        //fill post content
        onView(withId(R.id.postTitle)).perform(typeText("Test Post title"))

        //fill post content
        onView(withId(R.id.content_edit_text)).perform(typeText("Test Post content"))

        //select Project Request Post
        onView(withId(R.id.project_request_post_radio_button)).perform(click())

        //click add product button
        onView(withId(R.id.post_button)).perform(click())
    }

    @Test
    fun testShowMyProjects(){
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        //enter name
        onView(withId(R.id.userNameEditText)).perform(typeText("robi"))

        //enter password
        onView(withId(R.id.PasswordEditText)).perform(typeText("robi"))

        //click login button
        onView(withId(R.id.loginButton)).perform(click())

        //click add post(navigate to add post screen)
        onView(withId(R.id.myProjectsFragment)).perform(click())
    }

    @Test
    fun testEditPost() {
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        //enter name
        onView(withId(R.id.userNameEditText)).perform(typeText("robi"))

        //enter password
        onView(withId(R.id.PasswordEditText)).perform(typeText("robi"))

        //click login button
        onView(withId(R.id.loginButton)).perform(click())

        //click add post(navigate to add post screen)
        onView(withId(R.id.showMyPostFragment)).perform(click())

        //fill post content
        onView(withId(R.id.editText)).perform(typeText("Test edit Post title"))

        //fill post content
        onView(withId(R.id.editText2)).perform(typeText("Test editText2 Post content"))

        //select Project Request Post
        onView(withId(R.id.button)).perform(click())
    }

    @Test
    fun testShowMyRequests(){
        //Start Activity
        val activityScenario = ActivityScenario.launch(MainActivity::class.java)

        //enter name
        onView(withId(R.id.userNameEditText)).perform(typeText("robi"))

        //enter password
        onView(withId(R.id.PasswordEditText)).perform(typeText("robi"))

        //click login button
        onView(withId(R.id.loginButton)).perform(click())

        //click add post(navigate to add post screen)
        onView(withId(R.id.showRequestsFragment)).perform(click())
    }
}