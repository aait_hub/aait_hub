package com.example.aait_hub

import org.junit.Test
import org.junit.runner.RunWith
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4

@RunWith(AndroidJUnit4::class)
class AddPostFragmentTest {
    @Test
    fun testDisplayAddPostFragment() {

        val scenario = launchFragmentInContainer<AddPostFragment>()

        onView(withId(R.id.content_edit_text))
            .check(matches(isDisplayed()))

        onView(withId(R.id.post_type_radio_group))
            .check(matches(isDisplayed()))

        onView(withId(R.id.post_button))
            .check(matches(isDisplayed()))
    }
}