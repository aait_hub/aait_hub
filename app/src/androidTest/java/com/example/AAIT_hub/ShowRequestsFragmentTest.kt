package com.example.aait_hub

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ShowRequestsFragmentTest {
    @Test
    fun testDisplayShowRequestsFragment() {

        val scenario = launchFragmentInContainer<ShowRequestsFragment>()

        Espresso.onView(ViewMatchers.withId(R.id.showRequestsFragment))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}