package com.example.aait_hub

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ShowPostsFragmentTest {
    @Test
    fun testDisplayShowPostsFragment() {

        val scenario = launchFragmentInContainer<ShowPostsFragment>()

        onView(ViewMatchers.withId(R.id.ShowPostRecyclerView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}