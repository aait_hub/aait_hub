package com.example.aait_hub

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DetailPostFragmentTest {
    @Test
    fun testDisplayAddPostFragment() {

        val scenario = launchFragmentInContainer<DetailPostFragment>()

        onView(ViewMatchers.withId(R.id.detail_post_owner_name_textView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.detail_post_content_textView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.detail_post_likeButton))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.detail_post_voteButton))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.detail_post_joinButton))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}